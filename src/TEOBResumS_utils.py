#!/home/anuj.mishra/.conda/envs/TEOBResumS-cloned/bin/python

__author__ = 'Anuj Mishra'

""" 
TEOBResumS PyCBC waveform plugin.
Script to injectect eccentric aligned-spin WFs.
Some parts of this code have been borrowed from the following links and modified as per the need: 
https://bitbucket.org/eob_ihes/teobresums/src/master/PyCBC/teobresums.py
https://git.ligo.org/samson.leong/bilby-extra/-/blob/master/bilby-extra/eccentric/ecc_source.py

Note: In TEOBResumS, since WF generation in "FD" uses stationary phase approximation, it is not valid for non-zero eccentricities. 
Hence, we will only generate the WF in "TD" and then convert it to "FD" using PyCBC.

--------------
Documentation
--------------

# An example dict of parameters with default values:
init_prms = dict(f_start=10., f_low=20, f_high=None, f_ref=20., sample_rate=2048, wf_approximant="IMRPhenomXP", ifo_list = ['H1', 'L1', 'V1'])
cbc_prms =  dict(mass_1=m1, mass_2=m2, chi1z=chi1z, chi2z=chi2z, ecc=eccentricity,
            inclination=inclination, luminosity_distance=distance, 
            ra=0., dec=0., polarization=0., coa_phase=0., trig_time=0.,  
            mode_array=None, lambda1=None, lambda2=None, rwrap = 0.)
misc_prms = dict(save_data=False, data_outdir = './', data_label='data', data_channel='PyCBC-Injection')            
psd_prms = dict(Noise=True, psd_H1='O4', psd_L1='O4', psd_V1='O4')  # for no-noise, comment this line or set Noise=False.
where, if Noise == {True, 'True', 'true'} then noise will be added based on provided files for each detector, 
i.e., psd_H1 = Path to the file containing PSD information of H1 detector, and so on.

prms = {**init_prms, **cbc_prms, **psd_prms, **misc_prms}

prms : 
Dictionary of:

    Initial basic prms:

        * f_start : ({10., float}), optional 
            Starting frequency of the (2, 2) mode for waveform generation (in Hz). 
            Note that eccentricity will also be defined at this starting frequency.
        * f_low : ({20., float}), optional 
            Lower frequency considered for analysis (in Hz). For example, SNR calculation will use this f_low. 
        * f_high : ({None., float}), optional 
            Maximum frequency for matched filter computations (in Hz).     
        * f_ref : ({20., float}), optional  
            Reference frequency (in Hz) for specifying spins and inclination.. 
        * sample_rate : ({2048, int}), optional   
            Sample rate of WFs to generate. Default = 2048 (works for most of BBH parameters excpet when binary mass < ~20).
        * approximant : str, optional
            Name of LAL WF approximant to use for WF generation. Default="IMRPhenomXP".
        * ifo_list : list of strings
            List of interferometers to consider. Default =  ['H1', 'L1', 'V1']


    CBC Parameters:

        * mass_1 : float 
            The mass of the primary component object in the binary (in solar masses).
        * mass_2 : float 
            The mass of the secondary component object in the binary (in solar masses).
        * chi1z : float
            The z component of the first binary component. Default = 0.
        * chi2z : float
            The z component of the second binary component. Default = 0.            
        * ecc : float
            Eccentricity of the binary at a reference frequency of f_start.
        * mode_array : {None, list of lists}
            Modes to consider while generating WF. Default = None, contains all the modes available in the WF approximant. 
            For specific modes, use: mode_array = [[2,2], [4,4], etc.]    
        * lambda1 : float, optional
            The tidal deformability parameter of the first component object in the binary (in solar masses).
        * lambda2 : float, optional
            The tidal deformability parameter of the second component object in the binary (in solar masses).  
         

    Other (Extrinsic) Parameters:  
        * inclination : float
            Inclination (rad), defined as the angle between the orbital angular momentum L and the
            line-of-sight at the reference frequency. Default = 0.   
        * luminosity_distance : ({100.,float}), optional
            Luminosity distance to the binary (in Mpc).
        * ra : ({0.,float}), optional
            Right ascension of the source (in rad).
        * dec : ({0.,float}), optional
            Declination of the source (in rad).
        * polarization : ({0.,float}), optional
            Polarisation angle of the source (in rad).
        * coa_phase : ({0.,float}), optional
            Coalesence phase of the binary (in rad).
        * trig_time : ({0.,float}), optional
            Trigger time of the GW event (GPS time).
        * rwrap : ({0.,float}), optional
            Cyclic time shift value (in sec).

    Noise Parameters:

        * Noise : str, use either of these {True, 'True', 'true'} for setting up the noise.
            Boolian type value indicating whether to add Noise to the projected signals or not.
        * psd_H1, psd_L1, psd_V1 : str 
            Path to the respective PSD files. Default = 'O4' (PSDs corresponding to O4 are pre-saved and linked to the keyword `O4`.)
        * gen_seed : int
            Random seed for noise generation. Default=127.
            
    Misc Parameters:
        * save_data : str, use either of these {True, 'True', 'true'} for saving the data.
            A boolian type value to save the detector data as a frame file. Default = False.
        * data_outdir: str
            Output directory where data will be saved. Default = './', i.e., the current directory from where the command will be run.
        * data_label: str
            Data label to use while saving. Default = 'data'.
        * data_channel: str,
            Detector channel name (will be used while reading the data). Default = 'PyCBC_Injection'.
                
    """

import sys, os
sys.path.append('/home/anuj.mishra/git_repos/teobresums/')
from Python import EOBRun_module

import numpy as np
import pycbc
from pycbc.waveform import utils
from pycbc.types import TimeSeries
from pycbc.types import FrequencySeries
from pycbc.detector import Detector
import pycbc.psd
import pycbc.noise
import pycbc.noise.reproduceable

from copy import deepcopy

import os
cwd = os.path.dirname(__file__) + '/'

def modes_to_k(modes):
    """ Map (l, m) to linear index """
    return sorted([int(x[0]*(x[0]-1)/2 + x[1]-2) for x in modes])


def get_par(k, d):
    if k not in d.keys():
        raise ValueError("Need parameter {}.".format(k))
    if d[k] is None:
        raise ValueError("Need value for parameter {}.".format(k))
    return d[k] 


def teobresums_pars_update(pars):
    """ 
    Update TEOBResumS default parameters with pars. 
    Some defaults here may be different from that of the original defaults.
    For official documentation regarding various parameters, check the following link:
    https://bitbucket.org/eob_ihes/teobresums/wiki/Conventions,%20parameters%20and%20output 
    """

    initial_defaults = dict(f_start=10, sample_rate=None, delta_t=None, mode_array=None, lambda1=0., lambda2=0., chi1z=0., chi2z=0., trigger_time=None, taper=True)
    initial_defaults.update(pars)
    pars = initial_defaults.copy()
    
    m1 = get_par('mass_1',pars)
    m2 = get_par('mass_2', pars)
    m_tot = m1 + m2
    
    ## setting the sample rate
    # if pars['sample_rate']  is None, it will be decided based on the binary mass; either 2048 or 4096 unless 'delta_t' is provided.
    if pars['sample_rate'] is None:
        if pars['delta_t'] is None:
            if ((m_tot > 28) and (pars['ecc'])<0.3): # because f_RD_BBH(m_tot=28, q=1, chi1=0.99, chi2=0.99) ~ 960 Hz, which is close to the Nyquist frequency of 1024 Hz. And for higher eccentricities, signal length reduces dramatically.
                pars['sample_rate'] = 2048
            else:
                pars['sample_rate'] = 4096
        else:
            pars['sample_rate'] = 1./pars['delta_t']
    
    if pars['delta_t'] is None:
        pars['delta_t'] = 1./pars['sample_rate']    
    
    # Initial frequency and sampling
    flow = pars['f_start'] 
    srate = pars['sample_rate']
    
    # For generation in "TD"
    interp_uniform_grid = 1   # interpolate on grid with given dt

    # Some required parameters for TEOBResumS 
    m1          = get_par('mass_1',pars)
    m2          = get_par('mass_2', pars)
    chi1z       = get_par('chi1z', pars)
    chi2z       = get_par('chi2z', pars)
    lambda1     = get_par('lambda1', pars)
    lambda2     = get_par('lambda2', pars)
    distance    = get_par('luminosity_distance', pars)
    inclination = get_par('inclination', pars)
    coa_phase   = get_par('coa_phase', pars)
    ecc         = get_par('ecc', pars)

    if pars['mode_array'] is None:
        k = modes_to_k([(2,2)])
    else:
        k = modes_to_k(pars['mode_array'])    # will be overridden if 'output_lm' is explicitly provided
        if k[0] < 0 or k[-1] > 34:
            raise ValueError("Invalid mode list.")
    
    # Check masses convention
    q = m1/m2
    if q < 1.0 :
        m1,m2 = m2,m1
        chi1z,chi2z = chi2z,chi1z
        lambda1,lambda2 = lambda2,lambda1
        q = 1./q
    
    # Always use physical units
    pars['use_geometric_units'] = 0
                               
    ## Set TEOBResumS parameters
    # Below we list all possible parameters as a reference for an 
    # advanced used, though they are set to None and removed before
    # updating the dictionary with the use input 'pars'.
    # If those values are not set by user, then internal default are
    # used.                    
    defaults = {               
        ## Intrinsic parameters 
        'M' : m1+m2,                   # Total mass of the system [Msun]
        'q' : q,                       # Mass ratio of the system
        'LambdaAl2' : lambda1,         # Quadrupolar tidal parameter of body 1; For BH LambdaAl2=0; For NS LambdaAl2!=0.
        'LambdaBl2' : lambda2,         # Quadrupolar tidal parameter of body 2; For BH LambdaBl2=0; For NS LambdaBl2!=0.
        'chi1' : chi1z,                   # Aligned spin of body 1, overridden by chi1z
        'chi2' : chi2z,                   # Aligned spin of body 2, overridden by chi2z
        'chi1x': 0.,                   # x component of the spin of body 1
        'chi1y': 0.,                   # y component of the spin of body 1
        'chi1z': chi1z,                   # z component of the spin of body 1
        'chi2x': 0.,                   # x component of the spin of body 2
        'chi2y': 0.,                   # y component of the spin of body 2
        'chi2z': chi2z,                   # z component of the spin of body 2
                               
        ## Extrinsic parameters
        'distance' : distance,               # Distance of the source from Earth [Mpc] 
        'inclination' : inclination,         # Angle between the observer and the orbital angular momentum at the initial time [radians]
        'coalescence_angle' : coa_phase,     # Azimuthal angle entering the spherical harmonics decomposition [radians]; reference angle/phase at coalescence  
        #'trigger_time' : 0,      # time at the maximum strain amplitude; default=0 just like PyCBC timeseries.
        ## Waveform settings
        'domain' : 0,                         # 0 = TD, 1 = FD
        # 'r0' : None,                          # Initial radius (Compute from initial GW freq.)
        'initial_frequency': pars['f_start'], # in Hz if use_geometric_units = 0, else in geometric units
        'use_mode_lm' : k,                    # List of waveform modes to use
        'use_geometric_units': 0,             # I/O units output: 1 = geometric, 0 = physical
        
        ## Interpolate the dynamics and waveform
        # 1. if TD, and interp_uniform_grid:
        #   i. NOT use_geometric_units -> 1/dt = srate_interp
        #   ii. use_geometric_units    ->  dt  = dt_interp
        # 2. if FD, dt_interp is ignored & f_max = srate_interp / 2
        'srate_interp': srate,              # In Hz.
        'dt_interp': None,                  # In geometric unit. Default: 0.5
        'interp_uniform_grid':interp_uniform_grid,
        
        'df': 0.01,        # Frequency spacing for TEOBResumSPA, in the same units as initial_frequency. Freq axis goes from initial_frequency to srate_interp/2. in units of df.
        # 'interp_freqs': None,   # Flag to use a user-input list of frequency to interpolate TEOBResumSPA. Overrides df.
        # 'freqs': None,                # List of frequencies required by interp_freqs
        
        # ## Interpolation of the merger dynamics, after the peak of Omega.
        # #    - dt = dt_merger_interp
        # 'dt_merger_interp': None,           # In geometric unit. Default: 0.5
        
        ## Dynamics and waveform computed from ODE
        # If `ode_timestep=="Uniform"`:
        #   i. NOT use_geometric_units -> 1/dt = srate_interp
        #   ii. use_geometric_units    ->  dt  = dt_interp
        'srate': pars['sample_rate'],                      # In Hz.
        'dt': None,                         # In geometric unit. Default: 0.5
        
        # ## ODE
        # 'ode_timestep' : None,              # ODE timestep model. Options: ['adaptive', 'uniform', 'adaptive+uniform_after_LSO']
        # 'ode_abstol' : None,                # Absolute numerical tolerance.       Default: 1e-13
        # 'ode_reltol' : None,                # Relative numerical tolerance.       Default: 1e-11
        # 'ode_tmax' : None,                  # Maximum integration time.           Default: 1e9
        # 'ode_stop_radius' : None,           # Stop ODE evoluation at this r.      Default: 1.0
        # 'ode_stop_afterNdt' : None,         # Stop ODE evoluation N iterations
                                            # after the peak of orbital freq.     Default: 4
        # ## NQC stuff
        # 'nqc' : None,                       # How to set NQCs. Options: ['manual', 'auto', 'no']
        # 'nqc_coefs_flx' : None,             # NQC model used in the flux.       Defaults: {BBH/BHNS: nrfit_spin202002, BNS: None}
        # 'nqc_coefs_hlm' : None,             # NQC model used in the waveform.   Defaults: {BBH/BHNS: compute, BNS: None}
        
        # ## Tidal stuff
        # "tides": 0,
        # "tides_gravitomagnetic": 0,
        # 'use_tidal' : 0,                    # Tidal model.                 Defaults: {BBH: TIDES_OFF, BHNS: TEOBRESUM, BNS: TIDES_TEOBRESUM3} (see USETIDAL)
        # 'use_tidal_gravitomagnetic' : 0,    # Gravitomagnetic tides model. Defaults: {BBH/BHNS: TIDES_GM_OFF, BNS: TIDES_GM_PN}
        # 'pGSF_tidal' : None,                # GSF p-exponent.              Default = 4.0
        
        # ## Light ring and Last stable orbit
        # 'compute_LR' : 0,                   # Compute the light ring (LR)?
        # 'compute_LR_guess' : None,          # Initial guess of the LR
        # 'compute_LSO' : 0,                  # Compute the last stable orbit (LSO)?
        # 'compute_LSO_guess' : None,         # Iniital guess of the LSO
        
#         ## Post Adiabatic
#         'postadiabatic_dynamics' : 0,         # Postadiabatic approxiamation (PA)?
#         'postadiabatic_dynamics_N' : None,    # N-th order PA.                              Default: 8
#         'postadiabatic_dynamics_size' : None, # Guess for the size of the PA dynamics.      Default: 800
#         'postadiabatic_dynamics_stop' : None, # Stop after PA dynamics?                     Default: no
#         'postadiabatic_dynamics_rmin' : None, # Stop PA dynamics at this separation value.  Default: 14.0
        
        # ## Additional options
        # 'use_speedytail' : None,            # Speed up the computation of the tail factor in h_lm and F_lm? Default: No
        # 'centrifugal_radius' : None,        # Model for the centrifugal radius.                             Default: {BBH/BHNS: NLO, BNS: NNLO}
        # 'use_flm' : None,                   # Model for radiation reactoin.                                 Default: {BBH/BHNS: HM, BNS: SSNLO}
        # 'size' : None,                      # Initial guess of the size of the waveform.                    Default: 500
        # 'ringdown_extend_array' : None,     # Extend the ringdown array                                     Default: 500
        
        # ## Output Control
        # 'output_hpc' : 0,                   # Output plus and cross polarisations
        # 'output_multipoles' : 0,            # Output the waveform multipoles h_lm
        'output_lm' : k,                    # List of modes to output (if `output_multipoles`==True)
        'output_dynamics' : 0,              # Output the EOB dynamics
        'output_ringdown': 0,               # Output the ringdown
        'output_dir': "./data/",            # The output directory if any of the above is "yes", but it does not seem to be working
        'arg_out': 0,                       # Function output, return modes hlm/hflm. Default = 0 (no)

        ## Evolution
        # The following affect the evolution **only** if on teobresums-eccentric branch
        'ecc': ecc,                         # Eccentricity at the initial frequency.
        'ecc_freq': 2,                      # 0,1,2: "PERIASTRON", "AVERAGE", "APASTRON", None -> AVERAGE
        
        # # Hyperbolic settings
        # 'r_hyp': 0.,                        # Initial radius for hyperbolic orbits (0. -> r0 compute from fmin and ecc.)
        # 'H_hyp': None,                      # Initial energy for hyperbolic orbits
        # 'j_hyp': None,                      # Initial angular momentum for hyperbolic orbits
        }

    
    # Remove unset parameters (use internal defaults)
    # and return updated parameter list
    rm_none = {k: v for k, v in defaults.items() if v is not None}
    defaults.clear()
    defaults.update(rm_none)
    defaults.update(pars)
    pars = defaults.copy()
    return pars


def teobresums_td_pure_polarized_wf_gen(**pars):
    """ 
    Tapered Time-domain pure-polarised waveform generator using TEOBResumS WF model.
    
    """
   
    # TEOBResumS parameters
    pars = teobresums_pars_update(pars)
    
    #Run the WF generator
    t, Hp, Hc = EOBRun_module.EOBRunPy(pars)

    dt = t[1] - t[0]
    tmp_hp = TimeSeries(Hp, delta_t=dt)
    tmp_hc = TimeSeries(Hc, delta_t=dt)
    
    # Taper the WFs
    if pars['taper']:
        hp = utils.taper_timeseries(tmp_hp, tapermethod='TAPER_STARTEND', return_lal=False)  
        hc = utils.taper_timeseries(tmp_hc, tapermethod='TAPER_STARTEND', return_lal=False)
    else:
        hp, hc = tmp_hp, tmp_hc
        
    
    wf = hp - 1j*hc
    if pars['trigger_time'] is not None:
        epoch = pars['trigger_time'] - t[np.argmax(np.abs(wf))]
        wf.start_time = epoch
            
    return {'hp':wf.real(), 'hc':wf.imag()}  # Returned elements should be a pycbc time series for each GW polarization



def teobresums_fd_pure_polarized_wf_gen(**pars):
    """ 
    Frequency-domain pure-polarised waveform generator using TEOBResumS WF model.
    
    """
    
    # Generating TD WF
    wfs_res = teobresums_td_pure_polarized_wf_gen(**pars)
    hp, hc = wfs_res['hp'], wfs_res['hc']
        
    fd_hp = hp.to_frequencyseries(delta_f=hp.delta_f)
    fd_hc = hc.to_frequencyseries(delta_f=hc.delta_f)
    return {'fd_hp':fd_hp, 'fd_hc':fd_hc}


### Functions to generate injections ###

# replace zeros in psd with a tiny but non-zero number
def psd_remove_zero(psd):
    for i in range(len(psd)):
        if psd[i]==0:
            psd[i]=1e-52 
    return psd  

# function to generate psd 
def psd_gen(psd_file, psd_sample_rate = None, psd_duration = 32, **pars):
    # The PSD will be interpolated to the requested frequency spacing    
    delta_f = 1.0 / psd_duration
    length = int(psd_sample_rate / delta_f)
    low_frequency_cutoff = pars['f_low']
    psd = pycbc.psd.from_txt(psd_file, length, delta_f, low_frequency_cutoff, is_asd_file=False)
    psd = psd_remove_zero(psd)
    return psd
    
    
# Adds noise to a WF
def add_noise(wf, psd, noise_seed=127, **pars):  # takes a pure timeseries WF as input
    ##remove edge effects
    wf = pycbc.waveform.utils.taper_timeseries(wf, tapermethod='TAPER_STARTEND', return_lal=False)   
    ##generate a noise timeseries with duration equal to that of template
    # delta_t = wf.delta_t
    # t_samples = len(wf) 
    # ts = pycbc.noise.gaussian.noise_from_psd(t_samples, delta_t, psd, seed=noise_seed)  
    ts = pycbc.noise.reproduceable.colored_noise(psd, wf.start_time, wf.end_time, seed=noise_seed, sample_rate=wf.sample_rate, low_frequency_cutoff=pars['f_low'])  
    noisy_sig = pycbc.types.TimeSeries(np.array(wf)+np.array(ts), delta_t=wf.delta_t, epoch=wf.start_time) #adding noise to the pure wf
    return noisy_sig
     

# function to modify the starting of a WF so that it ends on an integer seconds + add extra length as specified by the user     
def wf_len_mod_start(wf, extra=1, **pars):
        sr = pars['sample_rate']
        olen = len(wf)   
        diff = wf.sample_times[0]-np.floor(wf.sample_times[0])  
        nlen = round(olen+sr*(extra+diff))
        dlen = int(round(sr*(extra+diff)))
        wf_strain = np.concatenate((np.zeros(dlen), wf))
        t0 = wf.sample_times[0]
        dt = wf.delta_t
        n = dlen
        tnn = t0-(n+1)*dt
        wf_stime = np.concatenate((np.arange(t0-dt,tnn,-dt)[::-1], np.array(wf.sample_times)))
        nwf = pycbc.types.TimeSeries(wf_strain, delta_t=wf.delta_t, epoch=wf_stime[0])
        return nwf

# Function to modify the end of a WF so that it ends on an integer GPS time (in sec) + add extra length as specified by the user.
def wf_len_mod_end( wf, extra=1, **pars): #post_trig_duration
    sr = pars['sample_rate']
    olen = len(wf)   
    dt = abs(wf.sample_times[-1] - wf.sample_times[-2])
    diff = np.ceil(wf.sample_times[-1]) - (wf.sample_times[-1] + dt)   #wf.sample_times[-1]-int(wf.sample_times[-1])  
    nlen = round(olen + sr*(extra+diff))
    wf.resize(nlen)
    return wf    

# Function to modify the length of a waveform so that its duration is a power of 2.
def make_len_power_of_2(wf):
    dur = wf.duration  
    wf.resize( int(round(wf.sample_rate * np.power(2, np.ceil( np.log2( dur ) ) ))) )
    #wf = cyclic_time_shift_of_WF(wf, rwrap = wf.duration - dur )
    wf = wf.cyclic_time_shift(wf.duration - dur)
    return wf
     

# Projects WFs onto detectors.
def sim_ecc_wf_gen(**pars):
    """ 
    Function to project signals onto detectors.
    
    """    
    # TEOBResumS parameters
    pars = teobresums_pars_update(pars)
    wfs_res = teobresums_td_pure_polarized_wf_gen(**pars)
    hp, hc = wfs_res['hp'], wfs_res['hc']

    # Choose a GPS end time, sky location, and polarization phase for the merger
    # NOTE: Right ascension and polarization phase runs from 0 to 2pi
    #       Declination runs from pi/2 to -pi/2 with the poles at pi/2 and -pi/2.

    trigger_time = get_par('trigger_time',pars)

    # end_time = trigger_time
    # hp.start_time += end_time
    # hc.start_time += end_time

    # projection onto detectors
    det = dict()
    ifo_signal = dict()
    for ifo in pars['ifo_list']:
        det[ifo] = Detector(ifo)
        ifo_signal[ifo] = det[ifo].project_wave(hp, hc, pars['ra'], pars['dec'], pars['polarization'])
        ifo_signal[ifo] = utils.taper_timeseries(ifo_signal[ifo], tapermethod='TAPER_STARTEND', return_lal=False)  #remove edge effects

    # We modify the length of the WF so that its time starts and ends in integer seconds. 
    # For this, we first make ends integer then add some extra seconds towards the end and the start.
    for ifo in pars['ifo_list']:
        wf = deepcopy( ifo_signal[ifo] )
        wf = wf_len_mod_start(wf, extra=1, **pars) 
        wf = wf_len_mod_end(wf, extra=1, **pars) # making post-merger duration of at least 2 seconds
        wf = make_len_power_of_2(wf)  # making total segment lenght a power of 2 by adding zeros towards the start of the WF.
        ifo_signal[ifo] = wf
    res = dict(pure_polarized_wfs={'hp':hp, 'hc':hc}, pure_ifo_signal=ifo_signal )    
    return res


# Adds noise to the projected WFs.
def sim_ecc_noisy_wf_gen(**pars):
    """ 
    Function to inject noisy signals using TEOBResumS WF model into detectors with a given noise profile.
    
    """    
    pars_default = dict(Noise=False, psd_H1='default', psd_L1='default', psd_V1='default', gen_seed=127,   # just a default random value
                    save_data=False, data_outdir = './', data_label= 'signal_data', data_channel='PyCBC_Injection')
    pars_default.update(pars)  
    pars = pars_default.copy()
    
    wfs_res = sim_ecc_wf_gen(**pars)
    pure_ifo_signal = wfs_res['pure_ifo_signal']
    
    if pars['psd_H1']=='default' or pars['psd_H1']=='O4':
        pars['psd_H1'] = cwd+'../data/PSDs/O4_target_psds/psd_aLIGO_O4high.txt'
   
    if pars['psd_L1']=='default' or pars['psd_L1']=='O4':
        pars['psd_L1'] = cwd+'../data/PSDs/O4_target_psds/psd_aLIGO_O4high.txt'

    if pars['psd_V1']=='default' or pars['psd_V1']=='O4':
        pars['psd_V1'] = cwd+'../data/PSDs/O4_target_psds/psd_aVirgo_O4high_NEW.txt' 

    psd_file_dict = dict(H1=pars['psd_H1'], L1=pars['psd_L1'], V1=pars['psd_V1'])
    psd = dict()
    for ifo in pars['ifo_list']:
        if psd_file_dict[ifo] != None:
            psd[ifo] = psd_gen(psd_file = psd_file_dict[ifo], psd_sample_rate=1./pure_ifo_signal[ifo].delta_t, 
            psd_duration = pure_ifo_signal[ifo].duration, **pars)
        else:
            psd[ifo] = None
            
    if pars['Noise']==True or pars['Noise']=='True' or pars['Noise']=='true':
        noisy_ifo_signal = dict()   
        for ifo in pars['ifo_list']:
            n_sig = add_noise(pure_ifo_signal[ifo], psd[ifo], noise_seed=pars['gen_seed'], **pars)  
            noisy_ifo_signal[ifo] = deepcopy(n_sig)  
    else:
        noisy_ifo_signal = pure_ifo_signal.copy()

    wfs_res.update(noisy_ifo_signal=noisy_ifo_signal, psd=psd)

    if pars['save_data']==True or pars['save_data']=='True' or pars['save_data']=='true':
        for ifo in pars['ifo_list']:
            print('Saving Data : %s'%ifo) 
            noisy_sig = wfs_res['noisy_ifo_signal'][ifo]
            data = pycbc.types.TimeSeries(np.array(noisy_sig), delta_t=1/noisy_sig.sample_rate, epoch=round(noisy_sig.sample_times[0]))
            frame.write_frame(pars['data_outdir'] + pars['data_label'] + '_' + ifo +'.gwf', ifo + ":" + pars['data_channel'], data) 
    return wfs_res  
    

# Returns projected WFs with noise and SNR values
def sim_ecc_noisy_wf_gen_with_snr(**pars):
    """ 
    Function to inject noisy signals using TEOBResumS WF model onto detectors with a given noise profile. It returns signals with and without noise, pure polarised WFs, 
    optimal and matched-filter SNR values in each detector, and corresponfing network SNR values as well. 
    
    """        
    wfs_res = sim_ecc_noisy_wf_gen(**pars)
    signal_templates = dict()
    match_filter_snr = dict()
    optimal_snr = dict()
    for ifo in pars['ifo_list']:
        signal = deepcopy( wfs_res['pure_ifo_signal'][ifo] )
        dt_end = signal.sample_times[-1] - pars['trigger_time'] + (signal.sample_times[1] - signal.sample_times[0]) 
        #signal_templates[ifo] = cyclic_time_shift_of_WF(signal, rwrap=dt_end)
        signal_templates[ifo] = signal.cyclic_time_shift(dt_end)
        #print(wfs_res['noisy_ifo_signal'][ifo], wfs_res['psd'][ifo])
        template, pure_data, noisy_data, psd = signal_templates[ifo], wfs_res['pure_ifo_signal'][ifo], wfs_res['noisy_ifo_signal'][ifo], wfs_res['psd'][ifo]
        mf_snr_ts = pycbc.filter.matchedfilter.matched_filter(template, noisy_data, psd=psd, low_frequency_cutoff=pars['f_low'], high_frequency_cutoff=pars['f_high'])
        match_filter_snr_val = max(np.abs(mf_snr_ts))
        opt_snr_ts = pycbc.filter.matchedfilter.matched_filter(template, pure_data, psd=psd, low_frequency_cutoff=pars['f_low'], high_frequency_cutoff=pars['f_high'])
        opt_snr_val = max(np.abs(opt_snr_ts))
        match_filter_snr[ifo] = match_filter_snr_val
        optimal_snr[ifo] = opt_snr_val

    network_optimal_snr = np.linalg.norm( np.array(list(optimal_snr.items()), dtype=object)[:,1] )
    network_matched_filter_snr =  np.linalg.norm( np.array(list(match_filter_snr.items()), dtype=object)[:,1] )

    wfs_res.update({'signal_templates': signal_templates, 'match_filter_snr': match_filter_snr, 'optimal_snr':optimal_snr})
    wfs_res.update({'network_optimal_snr': network_optimal_snr, 'network_matched_filter_snr': network_matched_filter_snr})
    return wfs_res
