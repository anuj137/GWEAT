.. GWMAT documentation master file, created by
   sphinx-quickstart on Mon Jan 30 14:18:27 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GWEAT's documentation!
=================================

.. include:: modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
