# GWEAT



## Introduction

GWEAT: Gravitational Wave Eccentricity Analysis Tools. 

Currently, GWEAT uses the TEOBResumS waveform model for studying eccentricity. It can be used to generate eccentric BBH injections, create frame files, and even perform parameter estimation, including eccentricity.

GWEAT is in the development phase, and for any issues, please create a Git issue.

## Documentation
Documentation can be found at this link: [GWEAT Documentation](https://anuj-mishra.gitlab.io/gweat/index.html).


